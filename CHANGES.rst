Changelog
=========

0.4 (unreleased)
----------------

- Nothing changed yet.


0.3 (2015-10-25)
----------------

- Move pipeline configurations into own directory pipelines.
  [thet]

- Restructure blueprints to be in blueprints directory and integrate orphaned
  blueprints from collective.blueprint.jsonmigrator.
  [thet]

- PEP 8.
  [thet, mauritsvanrees]

- Log json decode error instead of crashing [marciomazza]
